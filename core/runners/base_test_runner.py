from abc import ABC, abstractmethod


class BaseTestRunner(ABC):

    def __init__(self, reporter):
        self.reporter = reporter

    @abstractmethod
    def run_tests(self):
        pass
