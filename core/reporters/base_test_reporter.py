from abc import ABC, abstractmethod


class BaseTestReporter(ABC):

    @abstractmethod
    def report(self, data):
        pass
