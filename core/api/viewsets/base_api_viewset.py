from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin


class BaseAPIViewSet(GenericViewSet, ListModelMixin, RetrieveModelMixin):
    pass