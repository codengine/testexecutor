from core.api.serializers.base_serializer import BaseSerializer


class SerializerBuilder(object):

    @classmethod
    def build_dynamic_risk_serializer(self, serializer_name):

        def __init__(self, *args, **kwargs):
            fields = kwargs.get('fields', {})
            for field_name, serializer_field in fields.items():
                self.fields[field_name] = serializer_field

        DSerializer = type(str(serializer_name), (BaseSerializer,), {"__init__": __init__})
        return DSerializer