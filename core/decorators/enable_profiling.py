from functools import wraps


def enable_profiling(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        pass