from enum import Enum


class ExecutorTestStatus(Enum):
    PENDING = "pending"
    RUNNING = "running"
    COMPLETED = "completed"


class ExecutorTestResultStatus(Enum):
    PENDING = "pending"
    SUCCESS = "success"
    FAILED = "failed"


class APIRequestStatus(Enum):
    SUCCESS = 200
    ENV_BUSY = 300
    FAILED = 400
