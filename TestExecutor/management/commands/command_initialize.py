from django.core.management.base import BaseCommand

from TestExecutor.models.executor_test import ExecutorTest
from TestExecutor.models.executor_test_env import ExecutorTestEnv


class Command(BaseCommand):

    def handle(self, *args, **options):
        print("Initializing...")
        for i in range(1, 101):
            print("Creating Env %s" % i)
            if not ExecutorTestEnv.objects.filter(env_id=i).exists():
                env = ExecutorTestEnv(env_id=i, name="Env %s" % i)
                env.save()
                print("Created Env %s" % i)
            else:
                print("Skipping Env %s as already exists." % i)
        print("Initialization Done!")