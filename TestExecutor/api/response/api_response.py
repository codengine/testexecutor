class APIResponse(object):
    message = None
    socket_available = False
    socket_url = None
    polling_url = None

    def json(self):
        response = {}
        for k, v in self.__dict__.items():
            if v:
                response[k] = v
        return response