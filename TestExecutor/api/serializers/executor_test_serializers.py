import uuid

from TestExecutor.api.serializers.executor_test_result_serializers import ExecutorTestResultSerializer
from TestExecutor.models.executor_test import ExecutorTest
from TestExecutor.models.executor_test_result import ExecutorTestResult
from core.api.serializers.base_model_serializer import BaseModelSerializer


class ExecutorTestSerializer(BaseModelSerializer):

    def __init__(self, *args, **kwargs):
        super(ExecutorTestSerializer, self).__init__(*args, **kwargs)
        self.username = str(uuid.uuid4())

    def save(self, **kwargs):
        self.instance = super(ExecutorTestSerializer, self).save(**kwargs)
        self.instance.test_env.locked = True
        self.instance.test_env.save()
        executor_test_result = ExecutorTestResult()
        executor_test_result.save()
        self.instance.result_id = executor_test_result.pk
        self.instance.save()
        return self.instance

    class Meta:
        model = ExecutorTest
        fields = ('username', 'test_dir', "test_env")


class ExecutorTestListSerializer(BaseModelSerializer):
    class Meta:
        model = ExecutorTest
        fields = ('test_uuid', 'username', 'test_dir', "test_env", 'status')


class ExecutorTestDetailSerializer(BaseModelSerializer):
    result = ExecutorTestResultSerializer()

    class Meta:
        model = ExecutorTest
        fields = ('test_uuid', 'username', 'test_dir', "test_env", 'status', 'result')