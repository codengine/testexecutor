from TestExecutor.models.executor_test_result import ExecutorTestResult
from core.api.serializers.base_model_serializer import BaseModelSerializer


class ExecutorTestResultSerializer(BaseModelSerializer):
    class Meta:
        model = ExecutorTestResult
        fields = ('result', 'status')