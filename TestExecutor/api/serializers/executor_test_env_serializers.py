from rest_framework import serializers
from TestExecutor.models.executor_test_env import ExecutorTestEnv
from core.api.serializers.base_model_serializer import BaseModelSerializer


class ExecutorTestEnvListSerializer(BaseModelSerializer):
    env_id = serializers.SerializerMethodField()

    def get_env_id(self, obj):
        return obj.pk

    class Meta:
        model = ExecutorTestEnv
        fields = ('name', 'env_id', "locked")