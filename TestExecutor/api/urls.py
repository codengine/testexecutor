from django.conf.urls import url
from TestExecutor.api.viewsets.executor_test_api_viewset import ExecutorTestAPIViewset
from TestExecutor.api.viewsets.executor_test_env_viewset import ExecutorTestEnvAPIViewset

tests = ExecutorTestAPIViewset.as_view({'get': 'list', 'post': 'create'})
test_details = ExecutorTestAPIViewset.as_view({'get': 'retrieve'})
test_env_list = ExecutorTestEnvAPIViewset.as_view({'get': 'list'})


urlpatterns = [
    url(r'^tests/$', tests, name='executor_tests'),
    url(r'^test-env/$', test_env_list, name='executor_test_env'),
    url(r'^tests/(?P<test_uuid>[^/]+)/$', test_details, name='executor_test_details'),
]