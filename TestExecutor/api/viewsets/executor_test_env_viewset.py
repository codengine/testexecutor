from TestExecutor.api.serializers.executor_test_env_serializers import ExecutorTestEnvListSerializer
from TestExecutor.models.executor_test_env import ExecutorTestEnv
from core.api.viewsets.base_api_viewset import BaseAPIViewSet


class ExecutorTestEnvAPIViewset(BaseAPIViewSet):
    queryset = ExecutorTestEnv.objects.all()
    serializer_class = ExecutorTestEnvListSerializer

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        page = self.paginate_queryset(queryset)

        serializer_context = {'request': request}
        serializer = ExecutorTestEnvListSerializer(
            page, context=serializer_context, many=True
        )
        return self.get_paginated_response(serializer.data)