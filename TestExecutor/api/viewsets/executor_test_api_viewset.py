from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.response import Response
from rest_framework.reverse import reverse
from TestExecutor.api.response.api_response import APIResponse
from TestExecutor.api.serializers.executor_test_serializers import ExecutorTestSerializer, ExecutorTestListSerializer, \
    ExecutorTestDetailSerializer
from TestExecutor.models.executor_test import ExecutorTest
from TestExecutor.tasks import run_tests
from core.api.viewsets.base_api_viewset import BaseAPIViewSet


class ExecutorTestAPIViewset(BaseAPIViewSet):
    queryset = ExecutorTest.objects.all()
    serializer_class = ExecutorTestSerializer
    lookup_field = 'test_uuid'

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        page = self.paginate_queryset(queryset)

        serializer_context = {'request': request}
        serializer = ExecutorTestListSerializer(
            page, context=serializer_context, many=True
        )
        return self.get_paginated_response(serializer.data)

    @method_decorator(csrf_exempt)
    def create(self, request):
        try:
            executor_serializer = ExecutorTestSerializer(data=request.data)
            if executor_serializer.is_valid():
                test_env = executor_serializer.validated_data["test_env"]
                if test_env.is_busy():
                    api_response = APIResponse()
                    api_response.message = "Environment Busy."
                    return Response(api_response.json(), status=status.HTTP_200_OK)
                instance = executor_serializer.save()
                test_uuid = instance.test_uuid

                api_response = APIResponse()
                api_response.message = "Successful"
                api_response.socket_available = False
                api_response.socket_url = ""
                api_response.polling_url = reverse("executor_test_details", kwargs={"test_uuid": test_uuid}, request=request)

                test_dir = executor_serializer.validated_data["test_dir"]
                run_tests.delay(test_uuid, test_dir)

                return Response(api_response.json(), status=status.HTTP_201_CREATED)
            else:
                api_response = APIResponse()
                api_response.message = "Bad Request"
                return Response(api_response.json(), status=status.HTTP_400_BAD_REQUEST)
        except Exception as exp:
            api_response = APIResponse()
            api_response.message = "Bad Request"
            return Response(api_response.json(), status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def retrieve(self, request, test_uuid):
        queryset = ExecutorTest.objects.all()
        user = get_object_or_404(queryset, test_uuid=test_uuid)
        serializer = ExecutorTestDetailSerializer(user)
        return Response(serializer.data)
