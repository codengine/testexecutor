from pydoc import locate


def load_module(module_path):
    try:
        module = locate(module_path)
        return module
    except Exception as exp:
        pass