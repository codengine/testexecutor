import unittest
import time


class UnitTestResult(unittest.TestResult):

    def __init__(self, test_executor_object, stream=None, descriptions=None, verbosity=None):
        self.test_executor_object = test_executor_object
        self.testexecutor_result = test_executor_object.result
        self.tests_run = []
        super(UnitTestResult, self).__init__(stream=stream, descriptions=descriptions, verbosity=verbosity)

    def addError(self, test, err):
        time.sleep(2)
        super(UnitTestResult, self).addError(test=test, err=err)
        result_json = {self.testsRun: { "status": "Error", "test": test, "details": self._exc_info_to_string(err, test)}}
        result_text = str(result_json)
        existing_result = self.testexecutor_result.result
        existing_result = existing_result if existing_result else ""
        new_result = ",".join([existing_result, result_text])
        self.testexecutor_result.result = new_result
        self.testexecutor_result.save()
        self.tests_run.append(result_json)

    def addFailure(self, test, err):
        time.sleep(2)
        super(UnitTestResult, self).addFailure(test=test, err=err)
        result_json = {self.testsRun: {"status": "Failure", "test": test, "details": self._exc_info_to_string(err, test)}}
        result_text = str(result_json)
        existing_result = self.testexecutor_result.result
        existing_result = existing_result if existing_result else ""
        new_result = ",".join([existing_result, result_text])
        self.testexecutor_result.result = new_result
        self.testexecutor_result.save()
        self.tests_run.append(result_json)

    def addSuccess(self, test):
        time.sleep(2)
        super(UnitTestResult, self).addSuccess(test=test)
        result_json = {self.testsRun: {"status": "Success", "test": test}}
        result_text = str(result_json)
        existing_result = self.testexecutor_result.result
        existing_result = existing_result if existing_result else ""
        new_result = ",".join([existing_result, result_text])
        self.testexecutor_result.result = new_result
        self.testexecutor_result.save()
        self.tests_run.append(result_json)

    def get_test_report(self):
        return self.tests_run
