from TestExecutor.runners.unittest_result import UnitTestResult
from core.runners.base_test_runner import BaseTestRunner
import pathlib
import unittest


class UnittestTestRunner(BaseTestRunner):

    def __init__(self, test_dir=pathlib.Path(__file__).parent):
        self.loader = unittest.TestLoader()
        self.runner = unittest.TextTestRunner(test_dir)
        self.suite = self.loader.discover(test_dir)

    def run_tests(self, test_executor_object):
        test_result = UnitTestResult(test_executor_object)
        self.suite(test_result)
        return test_result.get_test_report()
