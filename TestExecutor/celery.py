from __future__ import absolute_import, unicode_literals

import os

from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')

# BROKER_URL = 'sqs://AKIAINDNPT4SFHW3FBYA:MkEsuz2cx1Z5e3dCKdrwU4Q3y2A94m0+AGn3r7+P@ap-southeast-1.elasticbeanstalk.com'

app = Celery('TestExecutor')

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
# app.config_from_object('django.conf:settings', namespace='CELERY')
app.config_from_object('django.conf:settings')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))