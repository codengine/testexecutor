from django.db import models
from core.models.base_entity import BaseEntity


class ExecutorTestItem(BaseEntity):
    test_code = models.TextField()