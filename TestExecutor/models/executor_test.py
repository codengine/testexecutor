import uuid
from django.db import models
from TestExecutor.constants.enums import ExecutorTestStatus
from TestExecutor.models.executor_test_env import ExecutorTestEnv
from TestExecutor.models.executor_test_item import ExecutorTestItem
from TestExecutor.models.executor_test_result import ExecutorTestResult
from core.models.base_entity import BaseEntity


class ExecutorTest(BaseEntity):
    test_uuid = models.CharField(max_length=1000)
    username = models.CharField(max_length=500)
    test_dir = models.CharField(max_length=800)
    tests = models.ManyToManyField(ExecutorTestItem)
    test_env = models.ForeignKey(ExecutorTestEnv, on_delete=models.CASCADE)
    status = models.CharField(max_length=255, default=ExecutorTestStatus.PENDING.value)
    result = models.ForeignKey(ExecutorTestResult, on_delete=models.CASCADE, null=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):

        if self.pk is None:
            uuid_4 = str(uuid.uuid4())
            self.test_uuid = uuid_4
        super(ExecutorTest, self).save(force_insert, force_update, using, update_fields)
