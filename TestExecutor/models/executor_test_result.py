from django.db import models

from TestExecutor.constants.enums import ExecutorTestResultStatus
from core.models.base_entity import BaseEntity


class ExecutorTestResult(BaseEntity):
    result = models.TextField(null=True)
    status = models.CharField(max_length=50, default=ExecutorTestResultStatus.PENDING.value)