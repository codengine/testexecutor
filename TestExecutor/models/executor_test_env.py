from django.db import models
from core.models.base_entity import BaseEntity


class ExecutorTestEnv(BaseEntity):
    name = models.CharField(max_length=500)
    env_id = models.IntegerField(choices=((i, i) for i in range(1, 101)))
    locked = models.BooleanField(default=False)

    def lock_env(self):
        self.locked = True
        self.save()

    def release_env(self):
        self.locked = False
        self.save()

    def is_busy(self):
        return self.locked
