from celery import task

from TestExecutor.constants.enums import ExecutorTestResultStatus, ExecutorTestStatus
from TestExecutor.decorators.loader_test_runner import load_test_runner
from TestExecutor.models.executor_test import ExecutorTest


@task()
@load_test_runner
def run_tests(test_uuid, test_dir, test_runner):
    test_executor_objects = ExecutorTest.objects.filter(test_uuid=test_uuid)
    if test_executor_objects.exists():
        test_executor_object = test_executor_objects.first()
        test_executor_result = test_executor_object.result
        test_executor_object.status = ExecutorTestStatus.RUNNING.value
        test_executor_object.save()
        test_report = test_runner.run_tests(test_executor_object)
        if all([tr[list(tr.keys())[0]]['status'] == 'Success' for tr in test_report]):
            test_executor_object.status = ExecutorTestResultStatus.SUCCESS.value
        else:
            test_executor_object.status = ExecutorTestResultStatus.FAILED.value
        test_executor_result.result = str(test_report)
        test_executor_result.save()
        test_executor_object.save()
