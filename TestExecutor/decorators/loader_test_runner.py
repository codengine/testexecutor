from functools import wraps
from django.conf import settings
from TestExecutor.loaders.class_loader import load_module


def load_test_runner(func):
    @wraps(func)
    def inner_wrapper(test_uuid, test_dir):
        Runner = load_module(settings.EXECUTE_TEST_RUNNER)
        if Runner:
            runner = Runner(test_dir)
        else:
            raise ModuleNotFoundError("Test Runner Not Found!")
        return func(test_uuid, test_dir, runner)
    return inner_wrapper
