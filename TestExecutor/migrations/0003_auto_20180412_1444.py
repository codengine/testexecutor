# Generated by Django 2.0.4 on 2018-04-12 14:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('TestExecutor', '0002_executortestenv_locked'),
    ]

    operations = [
        migrations.AlterField(
            model_name='executortestresult',
            name='result',
            field=models.TextField(null=True),
        ),
        migrations.AlterField(
            model_name='executortestresult',
            name='status',
            field=models.CharField(default='pending', max_length=50),
        ),
    ]
