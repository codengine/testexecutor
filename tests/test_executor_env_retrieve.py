from django.test import TestCase
from rest_framework.test import APIClient


class TestExecutorTestEnvRetrieve(TestCase):

    def setUp(self):
        pass

    def test_env_retrieve(self):

        client = APIClient()
        response = client.get('http://127.0.0.1:8081/api/v1/test-env/', {}, format='json')
        self.assertEqual(response.status_code, 200)