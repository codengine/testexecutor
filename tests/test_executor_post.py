from django.test import TestCase
from rest_framework.test import APIClient


class TestExecutorTestAPIPost(TestCase):

    def setUp(self):
        #Setup required for test database
        pass

    def test_post_201(self):
        data = {
            "username": "codengine",
            "test_dir": "/tests/shared/my_tests",
            "test_env": 1
        }

        client = APIClient()
        response = client.post('http://127.0.0.1:8081/api/v1/tests/', data, format='json')
        self.assertEqual(response.status_code, 201)

    def test_post_env_busy(self):
        data = {
            "username": "codengine",
            "test_dir": "/tests/shared/my_tests",
            "test_env": 1
        }

        client = APIClient()
        response = client.post('http://127.0.0.1:8081/api/v1/tests/', data, format='json')
        self.assertEqual(response.status_code, 200)

    def test_post_invalid_request(self):
        data = {
            "username": "codengine",
            "test_dir": "/tests/shared/my_tests",
            "test_env": None
        }

        client = APIClient()
        response = client.post('http://127.0.0.1:8081/api/v1/tests/', data, format='json')
        self.assertEqual(response.status_code, 400)

    def tearDown(self):
        pass